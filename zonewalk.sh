#!/bin/bash

usage() {
    echo "Usage:"
    echo " $0 <-d|--domain domain> [-s|--start name] [-o|--out output-file]"
    echo ""
    echo -e " Default output-file is 'domain.txt'\n"
    echo " You can controll process using command: tail -f 'domain.txt|output-file'"
    echo ""
    echo " 'drill' utility requires! "
    echo ""
    echo " 'drill' can show errors cause of random string in query sometimes during execution, it's ok. :)"
    exit 2
}

main() {
    NAME="$START" # record name with what search will start
    DOMAIN="$DOMAIN" # domain zone where search will be
    RCOUNTER=0 # record counter - control of singleness of NSEC chain.  
    
    # check existing of output file
    if [ ! -f $OUTFILE ]; then
        touch $OUTFILE
    fi
    
    while true; do
        echo "Query: $NAME.$DOMAIN  RCOUNTER: $RCOUNTER"

        RECORD=$(drill -D "$NAME.$DOMAIN" NSEC | awk -F'[[:space:]+]' '{ if ((NF>1)&&( $(NF-1)=="NSEC")) {print  $5 } }')
        echo "$RECORD"

        #If RECORD is exist in the OUTFILE generate random NAME
        if [ $(grep -c "$RECORD" "$OUTFILE") -gt 0 ]; then
            #echo "replica!"
            RAND_SYM="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9-_' | fold -w 5 | head -n 1)"
            NAME=$RAND_SYM
            RCOUNTER="$(expr $RCOUNTER + 1 )"
            # IF 50 times was no new records winish the algoritm
            if [ $RCOUNTER -gt 10 ]; then
               break
            fi
            continue
        else
            #echo "NEW RECORD ADDED!!!!"
            #RR=$(drill -D "$RECORD" ANY | grep -vE '(^;)' | awk 'NF > 0')
            #echo "$RR" >> "$OUTFILE.zone"
            echo "$RECORD" >> $OUTFILE
            NAME=$(echo "$RECORD" | awk -F'.' '{ print $1"0"}')
            RCOUNTER=0
        fi
    done
}

if [ "$#" -le 1 ]; then
    usage
fi

while [ "$#" != "0" ]
do
    case $1 in
        -h|--help)
            usage
            shift 1
            ;;

        -d|--domain)
            echo "falag: $1 value is $2"
            DOMAIN="$2"
            shift 2
            ;;
        -s|--start) 
            echo "falag: $1 value is $2"
            START="$2"
            shift 2
            ;;
        -o|--out)
            echo "falag: $1 value is $2"
            OUTFILE="$2"
            shift 2
            ;;

        -D|--database)
            echo "flag $1 value is $2"
            DATABASE=$2
            ;;
        *)
            echo "unknown parametr: $1"
            exit 2
            ;;
    esac
done

if [ "$DOMAIN" == '' ]; then echo "Domain Server not specified" usage

elif [ "$OUTFILE" == '' ]; then
    echo "Output file is $DOMAIN.txt"
    OUTFILE="$DOMAIN.txt"

elif [ "$START" == '' ]; then
    if [  -f $OUTFILE  -a  -n "$(tail -n1 $OUTFILE)"  ]; then 
      START="$(tail -n1 )"
   else
      START="a"
   fi
    echo "start with '$START'"
fi

STR=$(date +%s)
main
END=$(date +%s)
TIME=$(expr $END - $STR)
echo "zone: $DOMAIN Scan time total: $TIME seconds "
psql nsec3 -U nsec3 -c "insert INTO walk_stats (domain, timestamp, crawl_time, records_num) VALUES('$DOMAIN','$(date)','$TIME',$(wc -l $OUTFILE| cut -d' ' -f 1 )) "

